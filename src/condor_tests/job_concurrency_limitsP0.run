#! /usr/bin/env perl
##**************************************************************
##
## Copyright (C) 1990-2007, Condor Team, Computer Sciences Department,
## University of Wisconsin-Madison, WI.
## 
## Licensed under the Apache License, Version 2.0 (the "License"); you
## may not use this file except in compliance with the License.  You may
## obtain a copy of the License at
## 
##    http://www.apache.org/licenses/LICENSE-2.0
## 
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
##**************************************************************

use CondorTest;
use CondorUtils;
use Check::SimpleJob;
use Check::CondorLog;

my $testname = "job_concurrency_limitsP0.run";
my %children = ();
my $submitfile = "";
my $taskname = "";
my $burst = 0;
my $idletolerance = 6;
my %taskmorejobs = (
	"undefinedcolon" => 1,
	"checkxsw" => 2,
	"checksmall" => 3,
	"checklarge" => 2,
);

my $partitionable_condor_config = '
	DAEMON_LIST = MASTER,SCHEDD,COLLECTOR,NEGOTIATOR,STARTD
	ALL_DEBUG = 
	NUM_CPUS = 12
	# slot type 1: a traditional cpu-centric policy
	SLOT_TYPE_1 = cpus=100%,memory=100%,disk=100%
	SLOT_TYPE_1_PARTITIONABLE = True
	NUM_SLOTS_TYPE_1 = 1
	NEGOTIATOR_INTERVAL = 5
';

my $next_partitionable_condor_config = '
	DAEMON_LIST = MASTER,SCHEDD,COLLECTOR,NEGOTIATOR,STARTD
	ALL_DEBUG = 
	NUM_CPUS = 12
	# slot type 1: a traditional cpu-centric policy
	SLOT_TYPE_1 = cpus=100%,memory=100%,disk=100%
	SLOT_TYPE_1_PARTITIONABLE = True
	NUM_SLOTS_TYPE_1 = 1
	XSW_LIMIT = 0
	CONCURRENCY_LIMIT_DEFAULT = 0
	CONCURRENCY_LIMIT_DEFAULT_SMALL = 0
	CONCURRENCY_LIMIT_DEFAULT_LARGE = 0
	NEGOTIATOR_INTERVAL = 5
';

CondorTest::StartCondorWithParams(
    append_condor_config => $partitionable_condor_config,
	local_fresh => "TRUE",
);

sub multi_timeout_callback
{
	print "LogCheck timeout expired!\n";
	CondorTest::RegisterResult( 0, test_name, $testname );
	my $time = scalar(localtime());
	die "Log check timed out at $time - ?\n";
};


my $result = 0;
my $running_now = 0;
my $done = 0;
my $expect_idle = 0;
my $expect_run = 0;
my $expect_checks = 0;
my $total_checks = 8;
my $allow_too_few_idle_once = 0;

my $on_match = sub {
	my $response = shift;
	if($response eq "HitRetryLimit") {
		print "About to fail from timeout!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
	}
};

sub DelayOnNegotiator {
	print "Watching for another job to start up\n";
	# Started Negotiation Cycle A wait of about 10 seconds is caused here
	# leading the next one to occur in about 20 seconds
	#print scalar(localtime()) . " before multicheck\n";
	CondorLog::RunCheckMultiple(
		daemon => "Negotiator",
		match_regexp => "Started Negotiation Cycle",
		match_instances => 1,
		match_callback => $on_match,
		alt_timed => \&multi_timeout_callback,
		match_timeout => 80,
		match_new => "true",
		no_result => "true",
	);
	#print scalar(localtime()) . " After multicheck\n";
	# CondorTest::RemoveTimed(); done before returning from CondorLog::RunCheckMultiple
};

my $waitforitres = 0;

sub WaitForIt {
	my $count = 0;
	my $looplimit = 7;
	my $variance = 10;
	my $sleeptime = 0;
	my $res = 0;
	my $final = 0;
	while ($count < $looplimit) {
		$count += 1;
		#print "Loop $count in WaitForIt\n";
		system("condor_q");
		#print "Calling CheckStatus with final:$final\n";
		$res = CheckStatus($final);
		#print "Result from CheckStatus:$res\n";
		if($res == 1) {
			#print "WaitForIt got 1, we are really done\n";
			return(1);
		} elsif ($res == 2) {
			# wait on the next negotiator cycle to ensure
			# no more jobs start. Check one last time
			$final = 1;
			#print "Looking for the start of next negotiation cycle\n";
			DelayOnNegotiator();
			#print "BACK FROM Looking for the start of next negotiation cycle\n";
		} elsif ($res == -1) {
			print "WaitForIt got -1, something went wrong\n";
			return (-1);
		} elsif ($res == 0) {
			#print "We need more time in WaitForIt\n";
		}
		if( $count != $looplimit ) {
			$sleeptime = ($count * $variance);
			sleep($sleeptime);
			#print "sleep time set to $sleeptime\n";
		} else { 
			print "Timeout in WaitForIt\n";
			return(-1);
		}
	}
};

# return 0 needs more time
# return -1 for failure
# return 1 for happpy

sub CheckStatus {
	my $amidone = shift;
	$running_now = CountRunning();
	print "Running Now:$running_now ($expect_run) Idle:$idles ($expect_idle)\n";
	if($running_now > $expect_run) {
		# clearly unhappy
		print "Running jobs <$running_now> exceeded concurrency limits <$expect_run>\n";
		$expect_run = 1000; # remove will let a job start, bump count way up now.
		system("condor_rm -all");
		$done = 1;
		CondorTest::RegisterResult(0, "test_name", $testname);
		return(-1);
	}
	# compare current running to expected total
	if($running_now == $expect_run) {
		#print "Hit target for running jobs!\n";
		# if we expect no idle jobs, don't check.
		# remove jobs and return
		if($expect_idle == 0) {
			$done = 1;
			print "Expected idle 0 and run number met, remove jobs\n";
			system("condor_rm -all");
			#clearly done and happy
			return(1);
		} else {
			$idles = CountIdle($expect_idle);
			print "Running Now:$running_now ($expect_run) Idle:$idles ($expect_idle)\n";
			if($idles == $expect_idle) {
				$done = 1;
				print "Runs met and expected idle, About to remove these jobs\n";
				if($amidone == 1) {
					$expect_run = 1000; # remove will let a job start, bump count way up now.
					runToolNTimes("condor_q",1,0);
					system("condor_rm -all");
					return(1)
				} else {
					return(2);
				}
			} else {
				print "Run and Idle counts off running:$running_now idle:idles\n";
				return(0);
			}
		}
	} else {
		#print "running $running_now expecting $expect_run: not removing jobs\n";
		return(0);
	}
};

Condor::SetAllowedEvents("RegisterEvictedWithoutCheckpoint,RegisterSubmit,RegisterExecute,RegisterAbort");

sub on_submit {
};

sub on_imageupdated {
};


sub SetIdleTolerance {
	my $tolerance = shift;
	$allow_too_few_idle_once = $tolerance;
	print "Tolerance of no idle set:$allow_too_few_idle_once\n";
};

#Do a couple things before setting currency limits
print "First test basic job\n";
$result = SimpleJob::RunCheck();

my $append_submit_commands = '
concurrency_limits = XSW
';

# then test behaviour of non cuncurrency jobs first
# all from each batch should run. Remove and do next set.
my $count = 0;
$running_now = 0;
$done = 0;
$expect_checks = 0;
$expect_run = 3;
$expect_idle = 0;
$result = SimpleJob::RunCheck(
	on_submit => \&on_submit,
	on_imageupdated => \&on_imageupdated,
	test_name => $testname,
	no_wait => 1,
	queue_sz => 3,
	duration => 0,
	timeout => 600,
	append_submit_commands => $append_submit_commands
);
$waitforitres = WaitForIt();
print "First test basic job. 3 jobs no concurrency settings\n";
if($waitforitres == 1) {
	print "ok\n\n";
	RegisterResult(1,"test_name",$testname);
} else {
	print "bad\n\n";
	RegisterResult(0,"test_name",$testname);
}

# clean queue bwtween all checks
my @RES = ();
@RES = `condor_rm -all`;

# OK turn it on with concurrency limits
CondorTest::StartCondorWithParams(
    append_condor_config => $next_partitionable_condor_config,
	local_fresh => "TRUE",
);

$append_submit_commands = '
concurrency_limits = XSW
';

print "Test specified concurrency-XSW\n";
$running_now = 0;
my $queuesize = 0;
$done = 0;
$allow_too_few_idle_once = 6;
$expect_checks = 0;
$expect_run = 0;
$expect_idle = 3;
$taskname = "checkxsw";
$queuesize = 3;
$result = SimpleJob::RunCheck(
	test_name => $testname,
	no_wait => 1,
	queue_sz => $queuesize,
	duration => 0,
	timeout => 600,
	append_submit_commands => $append_submit_commands
);

$waitforitres = WaitForIt();
print "Test specified concurrency-XSW\n";
if($waitforitres == 1) {
	print "ok\n\n";
	RegisterResult(1,"test_name",$testname);
} else {
	print "bad\n\n";
	RegisterResult(0,"test_name",$testname);
}

$taskname = "";

# clean queue bwtween all checks
@RES = `condor_rm -all`;

$append_submit_commands = '
concurrency_limits = UNDEFINED:2
';

print "Test default concurrency and colon usage spec\n";
$running_now = 0;
$done = 0;
$allow_too_few_idle_once = 6;
$expect_checks = 0;
$expect_run = 0;
$expect_idle = 2;
$taskname = "undefinedcolon";
$queuesize = 2;
$result = SimpleJob::RunCheck(
	test_name => $testname,
	no_wait => 1,
	queue_sz => $queuesize,
	duration => 0,
	timeout => 600,
	append_submit_commands => $append_submit_commands
);
$taskname = "";;
$waitforitres = WaitForIt();
print "Test default concurrency and colon usage spec\n";
if($waitforitres == 1) {
	print "ok\n\n";
	RegisterResult(1,"test_name",$testname);
} else {
	print "bad\n\n";
	RegisterResult(0,"test_name",$testname);
}

# clean queue bwtween all checks
@RES = `condor_rm -all`;

$append_submit_commands = '
concurrency_limits = large.license
';

$running_now = 0;
$done = 0;
$allow_too_few_idle_once = 6;
$expect_checks = 0;
$expect_run = 0;
$expect_idle = 3;
print "Test class default: large.license\n";
$taskname = "checklarge";
$queuesize = 3;
$result = SimpleJob::RunCheck(
	test_name => $testname,
	no_wait => 1,
	queue_sz => $queuesize,
	duration => 0,
	timeout => 600,
	append_submit_commands => $append_submit_commands
);
$waitforitres = WaitForIt();
print "Test class default: large.license\n";
if($waitforitres == 1) {
	print "ok\n\n";
	RegisterResult(1,"test_name",$testname);
} else {
	print "bad\n\n";
	RegisterResult(0,"test_name",$testname);
}

# clean queue bwtween all checks
@RES = `condor_rm -all`;

$append_submit_commands = '
concurrency_limits = small.license
';

$running_now = 0;
$done = 0;
$allow_too_few_idle_once = 6;
$expect_checks = 0;
$expect_run = 0;
$expect_idle = 4;
print "Test class default: small.license\n";
$taskname = "checksmall";
$queuesize = 4;
$result = SimpleJob::RunCheck(
	test_name => $testname,
	no_wait => 1,
	queue_sz => $queuesize,
	duration => 0,
	timeout => 600,
	append_submit_commands => $append_submit_commands
);
$waitforitres = WaitForIt();
print "Test class default: small.license\n";
if($waitforitres == 1) {
	print "ok\n\n";
	RegisterResult(1,"test_name",$testname);
} else {
	print "bad\n\n";
	RegisterResult(0,"test_name",$testname);
}

CondorTest::EndTest();
exit(0);

sub CountRunning
{
	my $runcount = 0;
	my $line = "";
	my @goods = ();

	runCondorTool("condor_q",\@goods,2,{emit_output => 0});
	foreach my $job (@goods) {
		chomp($job);
		$line = $job;
		#print "JOB: $line\n";
		if($line =~ /^.*?\sR\s.*$/) {
			$runcount += 1;
			#print "Run count now:$runcount\n";
		} else {
			#print "Parse error or Idle:$line\n";
		}
	}
	return($runcount);
}

sub CountIdle
{
	my $expectidle = shift;
	my $idlecount = 0;
	my $line = "";
	my @goods = ();

	print scalar(localtime()) . " In count Idle:allow_too_few_idle_once=$allow_too_few_idle_once\n";
	#runcmd("condor_q");
	runCondorTool("condor_q",\@goods,2,{emit_output => 0});
	foreach my $job (@goods) {
		chomp($job);
		$line = $job;
		#print "JOB: $line\n";
		if($line =~ /^.*?\sI\s.*$/) {
			$idlecount += 1;
			print "Idle count now <$idlecount>, expecting <$expectidle>\n";
		}
	}
	if($allow_too_few_idle_once > 1) {
		# Case in point is a concurrency limit of one but two jobs
		# start. Triggering a fail on too few idle, could be failing
		# on a slow submit of the jobs. I'd rather fail on too many running
		# so the fist check gets a pass.
		# with sequential submits($burst = 0) submits happen much slower
		# and t6olerance of only 1 not suffecient
		#$allow_too_few_idle_once = 0;
		$allow_too_few_idle_once -= 1;
	} else {
		if($idlecount != $expectidle) {
			runToolNTimes("condor_q", 1, 1);
			die "Expected $expectidle idle but found $idlecount - die\n";
		}
	}

	return($idlecount);
}

sub ExamineSlots
{
	my $waitforit = shift;
	my $line = "";

	my $available = 0;
	my $looplimit = 24;
	my $count = 24; # go just once
	my @goods = ();
	if(defined $waitforit) {
		$count = 0; #enable looping with 10 second sleep
	}
	while($count <= $looplimit) {
		$count += 1;
		runCondorTool("condor_status",\@goods,2,{emit_output => 0});
		foreach my $job (@goods) {
			chomp($job);
			$line = $job;
			if($line =~ /^\s*Total\s+(\d+)\s*(\d+)\s*(\d+)\s*(\d+).*/) {
				#print "<$4> unclaimed <$1> Total slots\n";
				$available = $4;
			}
		}
		if(defined $waitforit) {
			if($available >= $waitforit) {
				last;
			} else {
				sleep 10;
			}
		} else{
		}
	}
	return($available);
}

sub ExamineQueue
{
	my $line = "";

	print "\nExpecting all jobs to be gone. Lets See.\n";
	my @goods = ();
	runCondorTool("condor_q",\@goods,2);
	foreach my $job (@goods) {
		chomp($job);
		$line = $job;
		print "JOB: $line\n";
		if($line =~ /^\s*(\d+)\s*jobs; .*$/) {
			$idlecount += 1;
			print "<$1> jobs still running\n";
		}
	}
	print "Total slots available here:\n\n";
	system("condor_status");
}

sub QueueMoreJobs
{
	my $submitfile = shift;
	my $taskname = shift;
	if($taskmorejobs{$taskname} > 0) {
		my $taskcount = $taskmorejobs{$taskname};
		#print "submitting $submitfile\n";
		#runToolNTimes("condor_submit $submitfile",1,0);
		my $pid = fork();

		if($pid == -1) {
            die "Fork error:$!\n";
        } elsif($pid == 0) {
			# we want the same callbacks!
			CondorTest::RunTest($testname,$submitfile,0);
			exit(0);
        } else {
            # parent gets pid of child fork
			# save pid
			$children{$pid} = 1;
        }
		$taskcount--;
		print "Additonal jobs wanted:$taskcount\n";
		if($taskcount == 0) {
			SetIdleTolerance($idletolerance);
		}
		$taskmorejobs{$taskname} = $taskcount;
	}
}
