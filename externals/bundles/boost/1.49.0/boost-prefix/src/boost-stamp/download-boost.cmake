message(STATUS "downloading...
     src='http://parrot.cs.wisc.edu/externals/boost_1_49_0.tar.gz'
     dst='/home/matt/programs/condor-8.3.7/bld_external/boost_1_49_0-p3/dl/boost_1_49_0.tar.gz'
     timeout='none'")




file(DOWNLOAD
  "http://parrot.cs.wisc.edu/externals/boost_1_49_0.tar.gz"
  "/home/matt/programs/condor-8.3.7/bld_external/boost_1_49_0-p3/dl/boost_1_49_0.tar.gz"
  SHOW_PROGRESS
  # no EXPECTED_HASH
  # no TIMEOUT
  STATUS status
  LOG log)

list(GET status 0 status_code)
list(GET status 1 status_string)

if(NOT status_code EQUAL 0)
  message(FATAL_ERROR "error: downloading 'http://parrot.cs.wisc.edu/externals/boost_1_49_0.tar.gz' failed
  status_code: ${status_code}
  status_string: ${status_string}
  log: ${log}
")
endif()

message(STATUS "downloading... done")
